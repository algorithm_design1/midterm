package com.soi.midalgo;

import java.util.ArrayList;
import java.util.Scanner;

public class MidAlgoA1_10 {

    static ArrayList<Integer> A = new ArrayList<>();
    static int n;

    static public void longSubArray() {
        int maxLength = 1, countLength = 1, start = 0;
        for (int i = 1; i < n; i++) {
            if (A.get(i) > A.get(i - 1)) {
                countLength++;
            } else {
                if (maxLength < countLength) {
                    maxLength = countLength;
                    start = i - maxLength;
                }
                countLength = 1;
            }
        }
        if (maxLength < countLength) {
            maxLength = countLength;
            start = n - maxLength;
        }
        System.out.print("Longest subarray of A: ");
        for (int i = start; i < maxLength + start; i++) {
            if (maxLength + start == 1) {
                System.out.println("All the same length.");
            } else {
                System.out.print(A.get(i) + " ");
            }
        }

    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("1.Enter size array  ");
        System.out.println("2.Enter data in array ");
        n = kb.nextInt();
        for (int i = 0; i < n; i++) {
            A.add(kb.nextInt());
        }
        longSubArray();
    }

}
