package com.soi.midalgo;

import java.util.ArrayList;
import java.util.Scanner;

public class MidAlgoA1_7 {

    static ArrayList<Integer> A = new ArrayList<>();
    static int c;

    static public boolean sum(int c) {
        for (int i = 0; i < A.size(); i++) {
            for (int j = i + 1; j < A.size(); j++) {
                if (A.get(i) + A.get(j) == c) {
                    System.out.println("Expected sum: " + c + " Num1: " + A.get(i) + " and Num2: " + A.get(j));
                    return true;
                }
            }
        }

        return false;

    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("1.Enter size array  ");
        System.out.println("2.Enter data in array (range (5*size = 1 to 5*n))  ");
        System.out.println("3.Enter expected sum (c) ");
        int n = kb.nextInt();
        for (int i = 0; i < n; i++) {
            A.add(kb.nextInt());
        }
        c = kb.nextInt();
        if (sum(c) == false) {
            System.out.println("No pairs that can be combined with sum(" + c + ")");
        }
    }
}
